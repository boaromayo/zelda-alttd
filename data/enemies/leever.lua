----------------------------------
--
-- Leever.
--
-- Start invisible and appear after a random time at a random position, then go to the hero direction.
-- The apparition point may be restricted to an area if the corresponding custom property is filled with a valid area, else the point will always be a visible one.
-- The area is the surface made by all other other entities with the same area property, except enemies.
-- Disappear after some time.
--
-- Properties : area
--
----------------------------------

-- Global variables
local enemy = ...
require("enemies/lib/common_actions").learn(enemy)
require("scripts/multi_events")

local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local camera = map:get_camera()
local sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
local quarter = math.pi * 0.5
local eighth = math.pi * 0.25
local area_entities = {}
local is_underground = true

-- Configuration variables
local area = enemy:get_property("area")
local walking_speed = 32
local walking_minimum_duration = 3000
local walking_maximum_duration = 5000
local waiting_minimum_duration = 2000
local waiting_maximum_duration = 3000

-- Returns a table filled with accepted area entites to move on, or camera if no area requested.
local function get_area_entities(area)

  local entities = {}

  if area then
    for entity in map:get_entities_in_region(enemy) do
      if entity:get_type() ~= "enemy" and entity:get_property("area") == area then
        table.insert(entities, entity)
      end
    end
  end

  -- Insert camera if no area found.
  if #entities == 0 then
    table.insert(entities, camera)
  end

  return entities
end

-- Get a random point over possible area.
local function get_random_point_in_area()

  local area_entity = area_entities[math.random(#area_entities)]
  local x, y, width, height = area_entity:get_bounding_box()

  return math.random(x, x + width), math.random(y, y + height)
end

-- Return the layer of the given position.
local function get_ground_layer(x, y)

  for ground_layer = map:get_max_layer(), map:get_min_layer(), -1 do
    if map:get_ground(x, y, ground_layer) ~= "empty" then
      return ground_layer
    end
  end
end

-- Make the enemy disappear.
local function disappear()

  sprite:set_animation("disappearing", function()
    is_underground = true
    enemy:restart()
  end)
end

-- Start the enemy movement.
local function start_walking()

  local movement = enemy:start_target_walking(hero, walking_speed)
  sol.timer.start(enemy, math.random(walking_minimum_duration, walking_maximum_duration), function()
    movement:stop()
    disappear()
  end)
end

-- Make the enemy appear at a random position.
local function appear()

  -- Postpone to the next frame if the random position would be over an obstacle.
  local x, y = enemy:get_position()
  local random_x, random_y = get_random_point_in_area()
  local layer = get_ground_layer(random_x, random_y)
  enemy:set_layer(layer or enemy:get_layer())
  if not layer or enemy:test_obstacles(random_x - x, random_y - y) then
    sol.timer.start(enemy, 10, function()
      appear()
    end)
    return
  end

  enemy:set_position(random_x, random_y)
  enemy:set_visible()
  sprite:set_animation("appearing", function()

    is_underground = false
    enemy:set_hero_weapons_reactions(2, {
      sword = 1,
      jump_on = "ignored"
    })
    enemy:set_can_attack(true)

    start_walking()
  end)
end

-- Wait a few time and appear.
local function wait()

  sol.timer.start(enemy, math.random(waiting_minimum_duration, waiting_maximum_duration), function()
    if not camera:overlaps(enemy:get_max_bounding_box()) then
      return true
    end
    appear()
  end)
end

-- Initialization.
enemy:register_event("on_created", function(enemy)

  enemy:set_life(2)
  enemy:set_size(16, 16)
  enemy:set_origin(8, 13)

  -- Get accepted area to move on.
  area_entities = get_area_entities(area)
end)

-- Restart settings.
enemy:register_event("on_restarted", function(enemy)

  -- States.
  enemy:set_damage(2)
  if is_underground then
    enemy:set_visible(false)
    enemy:set_can_attack(false)
    enemy:set_invincible()
    wait()
  else
    start_walking()
  end
end)

